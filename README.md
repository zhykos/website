# Zhykos' website
🌍 My french personal and professional website with static pages: my resume, my video game screenshots museum and other stuff.

👉 https://www.zhykos.fr 👈

## Landing page

Stack: HTML, JavaScript and CSS.

Template from: https://startbootstrap.com/theme/creative

## Pro webpage and gallery

Only the gallery is available in English.

Stack: React and Gatsby.

Template from: https://github.com/RyanFitzgerald/devfolio

Generated pages made with: NodeJs and TypeScript.
